FROM node:10.15.3

ENV PORT=80

WORKDIR /usr/src/app

COPY . .

RUN yarn install
RUN yarn build


EXPOSE 80
CMD [ "yarn", "start:prod" ]