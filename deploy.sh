#!/bin/sh

docker pull $IMAGE
docker tag $IMAGE $RELEASE_IMAGE
docker push $RELEASE_IMAGE
which ssh-agent || ( apk update && apk add openssh-client )
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh -o "StrictHostKeyChecking=no" $SSH_HOST << EOF 
  docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  docker pull $RELEASE_IMAGE
  docker rm -f $RELEASE_IMAGE_CONTAINER || true
  docker run --name $RELEASE_IMAGE_CONTAINER --restart=always -p 80:80 -d $RELEASE_IMAGE
EOF
